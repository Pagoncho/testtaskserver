#include <stdio.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include "thread.h"
#include "servlogint.h"
#include "tsqueue.h"
#include "tcpstream.h"
#include "conaccpt.h"
#include "concounter.h"

using namespace std;
/*
 * 
 */

class MyLogHandler : public ServerLoggerInterface
{
    public:
        MyLogHandler(ThreadSafeQueue<string>& tsqueue) : ServerLoggerInterface(tsqueue) 
        {
            ServerLoggerInterface::start();
        }
        
        static string getLoggingRecord(string msgType, string msgComment)
        {
            return  "[ " + printTimeNow() + " ]    " + msgType +
                  "    " + msgComment;    
        }    
     
    private:    
        void printLogMessage(const string* message)
        {
            cout << *message << endl;
        }
        
        static string printTimeNow()
        {
            timeval tp;
            gettimeofday(&tp, 0);
            time_t curtime = tp.tv_sec;
            tm *t = localtime(&curtime);
            
            char buffer[12]; //12 - length of the string exmp: 00:22:33:123 
            sprintf(buffer,"%02d:%02d:%02d:%03d", t->tm_hour, t->tm_min, t->tm_sec, (int)(tp.tv_usec/1000) );

            return string(buffer);
        }

};

class ConnectionHandler : public Thread
{
     TCPStream* tcpStream;
     ThreadSafeQueue<string>& logQueue;
     ConnectionCounter* connCount;
 
  public:
    ConnectionHandler(TCPStream* stream, ThreadSafeQueue<string>& queue, ConnectionCounter* cc) : 
      tcpStream(stream),logQueue(queue),connCount(cc) {}
 
    void* run() {
       
            char input[256];
            int len;
            while ((len = tcpStream->receive(input, sizeof(input)-1)) > 0 ){
                input[len] = (char)NULL; // NULL
                logQueue.add(MyLogHandler::getLoggingRecord("INFO", "RX: " + string(input)));
                tcpStream->send(input, len);
                logQueue.add(MyLogHandler::getLoggingRecord("INFO", "TX: " + string(input)));
            }
            
            connCount->decrementNConn();
            logQueue.add("connection is off");
            return NULL;
    }
};


string errMsg(string comment){
    return MyLogHandler::getLoggingRecord("ERROR",comment);
}

int main(int argc, char** argv) {
    /*handle the inputs*/
    string ip    = "127.0.0.1";
    int port     = 8888;
    int maxConn  = 10; /*maximum number of connection handlers*/
    int nConn    = 0; /*current number of connections*/
    
    ThreadSafeQueue<string>  logQueue; /*logging queue*/
    ConnectionCounter* cc = new ConnectionCounter();
    
    MyLogHandler* logHandler = new MyLogHandler(logQueue); /*handler that reads the messages from logging queue*/
    
    ConnectionAcceptor* connectionAcceptor = new ConnectionAcceptor(port,
                                                                    (char*)ip.c_str());
                                            
    if (!connectionAcceptor || connectionAcceptor->start() != 0) {
        logQueue.add(errMsg("Could not create an connection acceptor"));
        exit(1);
    }
    
    logQueue.add(MyLogHandler::getLoggingRecord("INFO","Server has started"));
        
    while (true) {
        TCPStream* connection = connectionAcceptor->connectionAccept(); 
        if (!connection) {
            logQueue.add(errMsg("Could not accept a connection"));
            continue;
        }
        
        cc->incrementNConn();
        
        if(nConn > maxConn){
            logQueue.add(errMsg("Too many connections: Connection is depreciated"));
            cc->decrementNConn();
            delete connection;
            continue;
        }
        
        ConnectionHandler* connHand = new ConnectionHandler(connection, logQueue, cc);
        if (!connHand) {
            logQueue.add(errMsg("Could not create ConnectionHandler " + cc->getNConn()));
            cc->decrementNConn();
            delete connection;
            continue;
        }
        
        stringstream temp;
        temp << connection->getPeerPort();
        temp << " with ID = ";
        temp << cc->getNConn();
        logQueue.add(MyLogHandler::getLoggingRecord("INFO","New connection from"     +
                                                           connection->getPeerIP()   + ":" +
                                                           temp.str()));
        connHand->start();
    }
}

