#ifndef CONACCPT_H
#define	CONACCPT_H

#include <string>
#include <netinet/in.h>
#include "tcpstream.h"

using namespace std;

class ConnectionAcceptor
{
    int    socketDesc;
    int    sPort;
    string sAddress;
    bool   isListening;
    
  public:
    ConnectionAcceptor(int port, const char* address="");
    ~ConnectionAcceptor();

    int        start();
    TCPStream* connectionAccept();

  private:
    ConnectionAcceptor() {}
};


#endif	/* CONACCPT_H */

