#ifndef THREAD_H
#define	THREAD_H

#include <pthread.h>
//pthread_mutex_t mutex_for_some_value = PTHREAD_MUTEX_INITIALIZER;

class Thread
{
  public:
    Thread();
    virtual ~Thread();

    int start();
    int join();
    int detach();
    pthread_t self();
    
    virtual void* run() = 0; /* to make class to be abstract (request for override) */
    
  private:
    pthread_t  tid;
    int        isRunning;
    int        isDetached;
    
};

#endif	/* THREAD_H */

