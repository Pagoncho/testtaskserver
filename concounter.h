#ifndef CONCOUNTER_H
#define	CONCOUNTER_H

#include <pthread.h>

class ConnectionCounter
{
    int nConn;
    pthread_mutex_t  mutex;

public:
    ConnectionCounter(){
        pthread_mutex_init(&mutex, NULL);
        nConn = 0;
    }
    ~ConnectionCounter(){
         pthread_mutex_destroy(&mutex);
    }
    
    void decrementNConn()
    {
        pthread_mutex_lock(&mutex);
        nConn--;
        pthread_mutex_unlock(&mutex);
    }

    void incrementNConn()
    {
        pthread_mutex_lock(&mutex);
        nConn++;
        pthread_mutex_unlock(&mutex);
    }
    
    int getNConn()
    {
        pthread_mutex_lock(&mutex);
        int temp = nConn;
        pthread_mutex_unlock(&mutex);
        return temp; 
    }
    
};



#endif	/* CONCOUNTER_H */

