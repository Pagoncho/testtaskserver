
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include "conaccpt.h"

#define MAX_PENDING_CONN 5   /*max pending connection for acceptance*/

ConnectionAcceptor::ConnectionAcceptor(int port, const char* address) 
    : socketDesc(0), sPort(port), sAddress(address), isListening(false) {} 

ConnectionAcceptor::~ConnectionAcceptor()
{
    if (socketDesc > 0) {
        close(socketDesc);
    }
}

int ConnectionAcceptor::start()
{
    if (isListening == true) {
        return 0;
    }

    //create socket
    socketDesc = socket(PF_INET, SOCK_STREAM, 0);
    struct sockaddr_in address;

    //init address structure
    memset(&address, 0, sizeof(address));
    address.sin_family = PF_INET;
    //revert bits of sPort to big-endian
    address.sin_port = htons(sPort);
    if (sAddress.size() > 0) {
        //convert IP to binary
        inet_pton(PF_INET, sAddress.c_str(), &(address.sin_addr));
    }
    else {
        address.sin_addr.s_addr = INADDR_ANY;
    }
    
    //set socket certain options to the socket
    //allow other sockets to bind to this port
    int optval = 1;
    setsockopt(socketDesc, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval); 
    
    //bind socket to the servers address
    int result = bind(socketDesc, (struct sockaddr*)&address, sizeof(address));
    if (result != 0) {
        perror("bind() failed");
        return result;
    }
    
    result = listen(socketDesc, MAX_PENDING_CONN);
    if (result != 0) {
        perror("listen() failed");
        return result;
    }
    isListening = true;
    return result;
}

TCPStream* ConnectionAcceptor::connectionAccept() 
{
    if (isListening == false) {
        return NULL;
    }

    struct sockaddr_in address;
    socklen_t len = sizeof(address);
    memset(&address, 0, sizeof(address));
    int sd = accept(socketDesc, (struct sockaddr*)&address, &len);
    if (sd < 0) {
        perror("accept() failed");
        return NULL;
    }
    return new TCPStream(sd, &address);
}