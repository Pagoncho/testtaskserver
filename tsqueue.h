#ifndef TSQUEUE_H
#define	TSQUEUE_H

#include <pthread.h>
#include <list>

using namespace std;


template <typename T> class ThreadSafeQueue
{
    list<T>          tsqueue; //queue list
    pthread_mutex_t  mutex;   //mutex
    pthread_cond_t   condv;   //conditional lock

  public:
    //initialization of locks      
    ThreadSafeQueue() {
        pthread_mutex_init(&mutex, NULL);
        pthread_cond_init(&condv, NULL);
    }
    ~ThreadSafeQueue() {
        pthread_mutex_destroy(&mutex);
        pthread_cond_destroy(&condv);
    }
    //lock the mutex while adding
    //and sending unlock signal for conditionally locked thread 
    void add(T item) {
        pthread_mutex_lock(&mutex);
        tsqueue.push_back(item);
        pthread_cond_signal(&condv);
        pthread_mutex_unlock(&mutex);
    }
    //lock mutex while removing
    //waiting for unlocking in case of conditional lock(if the queue is empty)
    T remove() {
        pthread_mutex_lock(&mutex);
        while (tsqueue.size() == 0) {
            pthread_cond_wait(&condv, &mutex);
        }
        T item = tsqueue.front();
        tsqueue.pop_front();
        pthread_mutex_unlock(&mutex);
        return item;
    }
    //return queue size (locked procedure) 
    int size() {
        pthread_mutex_lock(&mutex);
        int size = tsqueue.size();
        pthread_mutex_unlock(&mutex);
        return size;
    }
};


#endif	/* TSQUEUE_H */

