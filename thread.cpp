#include "thread.h"


static void* runThread(void* arg)
{
    return ((Thread*)arg)->run();
}

Thread::Thread() : tid(0), isRunning(0), isDetached(0) {}

//destructor: detach then cancel
Thread::~Thread()
{
    if (isRunning == 1 && isDetached == 0) {
        pthread_detach(tid);
    }
    if (isRunning == 1) {
        pthread_cancel(tid);
    }
}

//start thread execution
int Thread::start()
{
    //assign tid of the future thread
    //create thread with default params (NULL)
    //thread routine is runThread
    //routine parameters are referred to class Thread (this) 
    int result = pthread_create(&tid, NULL, runThread, this);
    if (result == 0) {
        isRunning = 1;
    }
    return result;
}

//wait for thread to complete
int Thread::join()
{
    int result = -1;
    if (isRunning == 1) {
        result = pthread_join(tid, NULL);
        if (result == 0) {
            isDetached = 1;
        }
    }
    return result;
}

//detach from thread to not wait for completion
int Thread::detach()
{
    int result = -1;
    if (isRunning == 1 && isDetached == 0) {
        result = pthread_detach(tid);
        if (result == 0) {
            isDetached = 1;
        }
    }
    return result;
}

//returns thread id
pthread_t Thread::self() {
    return tid;
}
