#ifndef SERVLOGINT_H
#define	SERVLOGINT_H

#include "tsqueue.h"
#include "thread.h"
#include <string>

class ServerLoggerInterface : public Thread
{
    ThreadSafeQueue<string>& tsqueue; // queue for synchronized output (in case of GUI...)
 
  public:
    ServerLoggerInterface(ThreadSafeQueue<string>& queue) : tsqueue(queue) {}
    virtual void printLogMessage(const string* message) = 0; 
 
    void* run() {
        
        while(true){
            string nextLogMsg = tsqueue.remove();
            printLogMessage(&nextLogMsg);
        }

        // Should never get here
        return NULL;
    }
    
};




#endif	/* SERVLOGINT_H */

