#include <arpa/inet.h>
#include "tcpstream.h"

//initialize stream with socket and dig out ip and port of peer
TCPStream::TCPStream(int sd, struct sockaddr_in* address) : socketDescr(sd) {
    char ip[50];
    //convert to char from binary 
    inet_ntop(PF_INET, (struct in_addr*)&(address->sin_addr.s_addr), ip, sizeof(ip)-1);
    peerIP = ip;
    //to little endian
    peerPort = ntohs(address->sin_port);
}

TCPStream::~TCPStream()
{
    close(socketDescr);
}

//writing to socket
ssize_t TCPStream::send(const char* buffer, size_t len) 
{
    return write(socketDescr, buffer, len);
}

//reading from socket and checks if there is timeout in connection
ssize_t TCPStream::receive(char* buffer, size_t len, int timeout) 
{
    if (timeout <= 0) return read(socketDescr, buffer, len);

    return connectionTimedOut;

}

string TCPStream::getPeerIP() 
{
    return peerIP;
}

int TCPStream::getPeerPort() 
{
    return peerPort;
}
